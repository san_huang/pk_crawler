# -*- coding: UTF-8 -*-
import os
import sys
import requests
import re
from re import sub
from decimal import getcontext, Decimal
from pprint import pprint
from bs4 import BeautifulSoup
from pktools import *
from base import Crawler
from dbstore import writeto591

class crawl591(Crawler):
    """docstring for Crawler"""
    def __init__(self, dbtabs, source="remote"):
        super().__init__(dbtabs)
        self.hasData=True
        # 暫時以既有欄位來記錄前次抓取分頁進度！
        self.nextFirstRow=dbtabs['crawllimit']
        self.assignSource=source
        self.CRAWLDATA=dict()
        # 計算目前實際取得數據筆數（後續改成以抓取數量來終止爬蟲）
        self.TOSTORENUMS=0
        # 用來判斷是否只顯示單筆結果！
        self.debug=True

    """
    每次crawler 591結果，配置不同頁面參數！
    1. 抓取下一筆作法！
    2. 暫存目前比數作法
    """
    def qRowsToJson(self, start=0):
        # 591固定參數
        perPageRows=20
        is_new_list="1"
        vtype=2
        searchtype=1
        region=0

        url=self.crawlURL+'index.php'

        # 暫時直接配置每次抓取次數（頁數）
        if self.debug:
            preTimes=1
        else:
            preTimes=5

        for i in range(0, preTimes):
            # 這樣可以assign 0
            if self.nextFirstRow is not None:
                firstRow=self.nextFirstRow
            # print('目前位置:%d 數據量:%d'%(firstRow, perPageRows))

            resp = requests.get(
                    url,
                    params={
                        'module':'search',
                        'action':'rslist',
                        'listview':'txt',
                        'is_new_list':is_new_list,
                        'type':vtype,
                        'searchtype':searchtype,
                        'region':region,
                        'firstRow':firstRow
                    },
                    cookies={
                       "listview": "txt",
                       'is_new_list':is_new_list,
                       'sessid':'3c5733f6d9ee77a4b5e2f064088e2315'
                    },
                    headers={
                    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Encoding':'gzip, deflate, sdch, br',
                    'Accept-Language':'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ja;q=0.2',
                    'Connection':'keep-alive',
                    'Cookie':'PHPSESSID=b60d373b90dfeeffe0270798d2bd2d13; user_sessionid=b60d373b90dfeeffe0270798d2bd2d13; urlJumpIp=1; urlJumpIpByTxt=%E5%8F%B0%E5%8C%97%E5%B8%82; listview=img; sessid=3c5733f6d9ee77a4b5e2f064088e2315; ba_cid=a%3A5%3A%7Bs%3A6%3A%22ba_cid%22%3Bs%3A32%3A%22cef45bebddd3395bd925d1020f875f00%22%3Bs%3A7%3A%22page_ex%22%3Bs%3A33%3A%22https%3A%2F%2Fsale.591.com.tw%2Findex.php%22%3Bs%3A4%3A%22page%22%3Bs%3A33%3A%22https%3A%2F%2Fsale.591.com.tw%2Findex.php%22%3Bs%3A7%3A%22time_ex%22%3Bi%3A1498154124%3Bs%3A4%3A%22time%22%3Bi%3A1498154462%3B%7D; _ga=GA1.3.361172749.1498154125; _gid=GA1.3.426663823.1498154125; _ga=GA1.4.361172749.1498154125; _gid=GA1.4.426663823.1498154125; __asc=df64d7cb15cd0f1c6564956e9e7; __auc=df64d7cb15cd0f1c6564956e9e7; _gat=1',
                    'Host':'sale.591.com.tw',
                    'Referer':'https://sale.591.com.tw/index.php',
                    'Upgrade-Insecure-Requests':'1',
                    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
                    }
                   )
            Json=resp.json()

            # soup = BeautifulSoup(Json['main'],"html.parser")

            self.HTMLCODE=Json['main']
            if not self.debug:
                self.parser("Json")
            else:
                # pprint(self.HTMLCODE)
                print('取得下一頁紀錄，並根據mysql.RECRAWL個數判斷多擷取數量！')

            # 上限次數
            totalRows=Json['count']
            page_soup=BeautifulSoup(Json['page'],"html.parser")
            pN=page_soup.find("a","pageNext")
            if not pN:
                return False
            elif i==(preTimes-1):
                # print('處理設定下次起始位置:%d'%self.nextFirstRow)
                self.hasData=False
                break
            else:
                self.nextFirstRow=int(pN.get("data-first"))
                if self.debug:
                    print('下次起始筆數:%d'%int(pN.get("data-first")))
        pass

    """
    act1. 直接產生soup解析物件
    act2. 組成物件變數（dist型態）
    act3. 決定導出json or mysql
    """
    def parser(self,output='Json'):
        # 從本機下載檔案進行解析測試！
        if self.assignSource=='local':
            with open(os.path.dirname(os.path.abspath(__file__))+'/sample/591_item.htm','r') as resp:
                self.HTMLCODE=resp.read()
            print("開始從本機檔案直接進行擷取")
        elif self.assignSource=='remote':
            if self.HTMLCODE is None:
                resp = requests.get(
                        url=self.crawlURL,
                        cookies={"listview": "txt"}
                       )
                self.HTMLCODE=resp.text
                if self.debug:
                    print("從網址[%s]進行擷取.." % (self.crawlURL))

        #根據html結構開始解析
        soup = BeautifulSoup(self.HTMLCODE, "html.parser")

        houses = soup.find_all("ul","shTxInfo")
        # 文字模式下單一物件項目
        for item in houses:
            # 城市
            city=item.find("li","rs").find("a","txt-sh-region").text
            self.ParserData["city"]=city
            # 行政區
            district=item.find("li","rs").find("a","txt-sh-section").text
            self.ParserData["district"]=district
            tmpstr=item.find("li","area").text.split('/',2)
            #總坪數/用途
            area = re.split('([\d|\.]+)坪', tmpstr[0])
            if len(area)>1:
                self.ParserData["area"]=area[1]
            usefor=tmpstr[1]
            self.ParserData["usefor"]=usefor
            # 物件頁面連結
            url=item.find("li","address").find("a").get('href')
            if url is not None:
                self.parserItemPage(self.crawlURL+url)
            # 總價 @see http://stackoverflow.com/questions/8421922/how-do-i-convert-a-currency-string-to-a-floating-point-number-in-python
            price=float(Decimal(sub(r'[^\d.]', '', item.find("li","price").find("strong").text)))
            self.ParserData["price_total"]=price
            # 瀏覽數(選用欄位)
            visited=int(item.find("li","visited").text)
            self.ParserData["visited"]=visited
            # print(visited)
            # 更新時間(選用欄位)
            updatemoment=item.find("li","update").find("strong").find("strong").text
            self.ParserData["updatemoment"]=updatemoment
            # print(updatemoment)
            if not self.ParserData:
                print("單筆記錄解析資料錯誤，終止程式!")
                sys.exit(1)
            elif self.debug:
                pprint(self.ParserData)
                self.CRAWLDATA[self.ParserData["title"]+url]=self.ParserData
                break
            else:
                # 加入判斷若該筆資料已經有存在於爬蟲結果則不在寫入！
                if self.ParserData["title"]+url not in self.CRAWLDATA.keys():
                    self.TOSTORENUMS+=1
                self.CRAWLDATA[self.ParserData["title"]]=self.ParserData
                # self.CRAWLDATA[title+url]=self.ParserData
                # print('now have %d datas..'%self.TOSTORENUMS)
                self.ParserData=dict()

        # pprint(self.CRAWLDATA)
        # sys.exit()
        if not self.CRAWLDATA:
            print("集合解析資料暫存錯誤，終止程式!")
        elif self.hasData:
            print("持續擷取下一頁，暫存本業結果寫入CRAWLDATA")
        else:
            func = getattr(self, "writeTo"+output)
            func()
            print("已將爬蟲結果寫入output!")
        return 1


    """
        解析物件內容頁面欄位
    """
    def parserItemPage(self, toUrl):
        # 從本機下載檔案進行解析測試！
        if self.assignSource=='local':
            with open(os.path.dirname(os.path.abspath(__file__))+'/sample/591_detail.htm','r') as resp:
                soup = BeautifulSoup(resp.read(),"html.parser")
            if self.debug:
                print("開始從本機檔案直接進行擷取")
        elif self.assignSource=='remote':
            self.ParserData["url_houseitem"]=toUrl
            # 導向舊版
            toOldURL=toUrl+'?v=old'
            resp = requests.get(
                        toOldURL,
                        cookies={

                        },
                        headers={
                        'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                        'Accept-Encoding':'gzip, deflate, sdch, br',
                        'Accept-Language':'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ja;q=0.2',
                        'Connection':'keep-alive',
                        'Cookie':'PHPSESSID=b60d373b90dfeeffe0270798d2bd2d13; user_sessionid=b60d373b90dfeeffe0270798d2bd2d13; urlJumpIp=1; urlJumpIpByTxt=%E5%8F%B0%E5%8C%97%E5%B8%82; listview=img; sessid=3c5733f6d9ee77a4b5e2f064088e2315; ba_cid=a%3A5%3A%7Bs%3A6%3A%22ba_cid%22%3Bs%3A32%3A%22cef45bebddd3395bd925d1020f875f00%22%3Bs%3A7%3A%22page_ex%22%3Bs%3A33%3A%22https%3A%2F%2Fsale.591.com.tw%2Findex.php%22%3Bs%3A4%3A%22page%22%3Bs%3A33%3A%22https%3A%2F%2Fsale.591.com.tw%2Findex.php%22%3Bs%3A7%3A%22time_ex%22%3Bi%3A1498154124%3Bs%3A4%3A%22time%22%3Bi%3A1498154462%3B%7D; _ga=GA1.3.361172749.1498154125; _gid=GA1.3.426663823.1498154125; _ga=GA1.4.361172749.1498154125; _gid=GA1.4.426663823.1498154125; __asc=df64d7cb15cd0f1c6564956e9e7; __auc=df64d7cb15cd0f1c6564956e9e7; _gat=1',
                        'Host':'sale.591.com.tw',
                        'Referer':'https://sale.591.com.tw/index.php',
                        'Upgrade-Insecure-Requests':'1',
                        'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
                        }
                   )
            soup = BeautifulSoup(resp.text,"html.parser")
            if self.debug:
                print("[parserItemPage]進行物件內容擷取")

        # 完整標題
        title=soup.find("div",{"id":"infoTitle"}).find("h1")
        title.a.decompose()
        # 排除表情字元
        self.ParserData["title"]= remove_emoji(title.text)

        # 主要圖片
        cover=soup.find("div",{"id":"j_image_viewer"})
        if cover is not None:
            coverimg=cover.find("img").get('src')
            self.ParserData["cover_src"]=coverimg
            if cover.get('width') is not None:
                self.ParserData["cover_width"]=cover.get('width')
            if cover.get('height') is not None:
                self.ParserData["cover_height"]=cover.get('height')
        # pprint(cover.get('src'))
        dinfo=soup.find("ul",{"id":"attr"})

        # 單價
        if len(dinfo.findAll("li")) > 1:
            singleprice=float(Decimal(sub(r'[^\d.]', '', dinfo.findAll("li")[1].findAll("span")[1].find("em").text)))
            self.ParserData["princ_single"]=singleprice
        # 格局(尚未個別拆解) dinfo.findAll("li")[2].findAll("span")[1].text
        tmppattn=dinfo.findAll(text='格局：')
        if len(tmppattn) is not 0:
            patterns=tmppattn[0].parent.parent.findAll("span")[1].text
            self.ParserData["patterns"]=patterns
            # 格局拆解
            ps = re.split('(\d+)房', patterns)
            if len(ps)>1:
                self.ParserData['patterns_room'] = ps[1]
            ps = re.split('(\d+)廳', patterns)
            if len(ps)>1:
                self.ParserData['patterns_hall'] = ps[1]
            ps = re.split('(\d+)衛', patterns)
            if len(ps)>1:
                self.ParserData['patterns_bathroom'] = ps[1]
            ps = re.split('(\d+)陽台', patterns)
            if len(ps)>1:
                self.ParserData['patterns_balcony'] = ps[1]

        # 型態/現況 dinfo.findAll("li")[6].findAll("span")[1].text
        STAT=dinfo.findAll(text='型態/現況：')
        if len(STAT) is not 0:
            stattmp=STAT[0].parent.parent.findAll("span")[1].text
            # pprint(stattmp)
            stats=stattmp.split('/')
            # pprint(stats)
            housetype=stats[0]
            self.ParserData["house_type"]=housetype
            if len(stats) > 1:
                houseusefor=stats[1]
                self.ParserData["house_usefor"]=houseusefor

        # 樓層 Fs=dinfo.findAll("li")[4].findAll("span")[1].text
        Fs=dinfo.findAll(text='樓層：')
        if len(Fs) is not 0:
            floors=Fs[0].parent.parent.findAll("span")[1].text.split('/')
            atfloor=floors[0].strip('F')
            self.ParserData["floor_at"]=atfloor
            # 樓層(floor_at=整棟，型態:廠辦無設定！)
            if self.ParserData['floor_at']=='整棟' and self.ParserData['house_type']=='':
                if re.search('[土|農|工業用]地',self.ParserData['title']) or self.ParserData['usefor']=='土地':
                    self.ParserData['house_type']=='土地'
                elif self.ParserData['usefor']=='廠房':
                    self.ParserData['house_type']=='廠辦'

            if len(floors) > 1:
                totalfloor=floors[1].strip('F')
                self.ParserData["floor_total"]=totalfloor

        # 屋齡 houseage=dinfo.findAll("li")[5].findAll("span")[1].text
        HA=dinfo.findAll(text='屋齡：')
        if len(HA) is not 0:
            houseage=HA[0].parent.parent.findAll("span")[1].text
            self.ParserData["houseage"]=houseage
            # 屋齡計算數值
            ha=0
            ps1 = re.split('(\d+)年', houseage)
            if len(ps1)>1:
                ha+=int(ps1[1])
            ps2 = re.split('(\d+)個月', houseage)
            if len(ps2)>1:
                ha+=float(int(ps2[1])/12)
            self.ParserData['houseage_count']=round(ha,2)

        # 附加欄位資訊（詳細資料下的其他資料，從字串取出全部坪數進行計算其他坪數！）
        dExtinfo=soup.find("div",{"id":"sale-other-info"})
        if dExtinfo.find("div","areas") is not None:
            areastr=dExtinfo.find("div","areas").text
            ac=0
            acs = re.split('主建物([\d|\.]+)坪', areastr)
            if len(acs)>1:
                ac+=float(acs[1])
            acs = re.split('附屬建物([\d|\.]+)坪', areastr)
            if len(acs)>1:
                ac+=float(acs[1])
            acs = re.split('共用部分([\d|\.]+)坪', areastr)
            if len(acs)>1:
                ac+=float(acs[1])
            self.ParserData["area_woparking"]=round(ac,2)

        # 有無車位(可進一步解析類型、有無)
        # parking=dinfo.findAll("li")[7].findAll("span")[1].text
        PK=dinfo.findAll(text='車位：')
        if len(PK) is not 0:
            parking=PK[0].parent.parent.findAll("span")[1].text
            self.ParserData["parking"]=u''.join(parking.split())
            arparking = re.split('([\d|\.]+)坪', self.ParserData["parking"])
            if len(arparking)>1:
                self.ParserData['area_parking']=round(float(arparking[1]),2) # float(arparking[1])
                area_val=Decimal(float(self.ParserData['area'])-float(arparking[1]), getcontext())
                areamain=float(area_val.__round__(2))
                self.ParserData['area_woparking']=areamain

        # 社區
        # community=dinfo.findAll("li")[8].findAll("span")[1].text
        cmt=dinfo.findAll(text='社區：')
        if len(cmt) > 0:
            community=cmt[0].parent.parent.findAll("span")[1].text
            self.ParserData["community"]=community

        # 部分地址欄位p.view_map_address
        addressSeg=soup.find("p","view_map_address")
        if addressSeg is not None:
            self.ParserData["addressSegment"]=addressSeg.text.replace('地址：','')

        # 地圖網址
        # 街景模式網址
        mstreeturl=soup.find("div",{"id":"streetRound"}).find("iframe").get('src')
        self.ParserData["url_streetmap"]=self.crawlURL+mstreeturl
        # 一般模式網址
        murl=soup.find("div",{"id":"mapRound"}).find("iframe").get('src')
        if murl:
            mapurl=murl.replace('detail=detail','s=j_edit_maps')
            self.parserMapPage(self.crawlURL+mapurl)
        pass

    """
        解析物件內容頁面導向的地圖頁面欄位
    """
    def parserMapPage(self, toMapUrl):
        self.ParserData["url_housemap"]=toMapUrl
        resp = requests.get(
                            toMapUrl,
                            cookies={},
                            headers={
                            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                            'Accept-Encoding':'gzip, deflate, sdch, br',
                            'Accept-Language':'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ja;q=0.2',
                            'Connection':'keep-alive',
                            'Cookie':'PHPSESSID=b60d373b90dfeeffe0270798d2bd2d13; user_sessionid=b60d373b90dfeeffe0270798d2bd2d13; urlJumpIp=1; urlJumpIpByTxt=%E5%8F%B0%E5%8C%97%E5%B8%82; listview=img; sessid=3c5733f6d9ee77a4b5e2f064088e2315; ba_cid=a%3A5%3A%7Bs%3A6%3A%22ba_cid%22%3Bs%3A32%3A%22cef45bebddd3395bd925d1020f875f00%22%3Bs%3A7%3A%22page_ex%22%3Bs%3A33%3A%22https%3A%2F%2Fsale.591.com.tw%2Findex.php%22%3Bs%3A4%3A%22page%22%3Bs%3A33%3A%22https%3A%2F%2Fsale.591.com.tw%2Findex.php%22%3Bs%3A7%3A%22time_ex%22%3Bi%3A1498154124%3Bs%3A4%3A%22time%22%3Bi%3A1498154462%3B%7D; _ga=GA1.3.361172749.1498154125; _gid=GA1.3.426663823.1498154125; _ga=GA1.4.361172749.1498154125; _gid=GA1.4.426663823.1498154125; __asc=df64d7cb15cd0f1c6564956e9e7; __auc=df64d7cb15cd0f1c6564956e9e7; _gat=1',
                            'Host':'sale.591.com.tw',
                            'Referer':'https://sale.591.com.tw/index.php',
                            'Upgrade-Insecure-Requests':'1',
                            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
                            }
                )
        soup = BeautifulSoup(resp.text,"html.parser")
        # with open(os.path.dirname(os.path.abspath(__file__))+'/sample/591_map.htm','r') as resp:
        #     soup = BeautifulSoup(resp.read(),"html.parser")

        # 物件經度
        lat=soup.find("input",{"id":"lat"}).get('value')
        self.ParserData["lat"]=float("%.6f"%float(lat))
        # 物件緯度
        lng=soup.find("input",{"id":"lng"}).get('value')
        self.ParserData["lng"]=float("%.6f"%float(lng))
        pass

    """
        配置單一記錄擷取並解析後資料結構以利未來修改與調整
    """
    def getParseData(self):
        '''
        distdata =
        '''
        return distdata


if __name__ == "__main__":
    mysql = writeto591()
    for item in mysql.getAllTabs('591'):
        cr=crawl591(item,'remote')
        cr.debug=False
        cr.qRowsToJson()
        cr.writeToJson()
        # 紀錄最新的起始列數，將目前self.nextFirstRow寫入db(由mysql呼叫處理)
        mysql.updateCrawllimit('591', cr.nextFirstRow)
        mysql.checkifCrawldata(cr.CRAWLDATA)
        mysql.distToRaw('crawldata_591')
        mysql.toCrawldata()
        mysql.close()
        # mysql.updateFirstrow(item['tabsId'], cr.nextFirstRow)
    sys.exit(0)

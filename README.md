Personalwork版本 crawler
==========

A crawler for web PTT

參考PTTcrawler，開發專門爬蟲PTT-web頁面工具

###執行環境
python3

###example
--------------

    $ python3 mycrawler.py


###Task
- 可以讀取mysql-schema取得請求的板塊轉換成對應網址，同時根據板塊指定儲存檔名為特定路徑下[m-d-hi-xxx.json]
    - python mysql連線(ok)
    - 指定路徑(ok)
    - 配置存檔名稱(ok)
- 不需設定頁碼(選擇性處理)，直接限制取得最新往前推算3頁(根據參數設定來判斷)
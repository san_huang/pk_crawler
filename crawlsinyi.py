# -*- coding: UTF-8 -*-
import os
import sys
import requests
from random import randint
import re
from re import sub
from decimal import getcontext, Decimal
from pprint import pprint
from bs4 import BeautifulSoup
from pktools import *
from base import Crawler
from dbstore import writetosinyi

class crawlsinyi(Crawler):
    """docstring for Crawler"""
    def __init__(self, dbtabs, source="remote"):
        super().__init__(dbtabs)
        self.hasData=True
        # 暫時以既有欄位來記錄前次抓取分頁進度！
        self.nextFirstRow=dbtabs['crawllimit']
        self.assignSource=source
        self.CRAWLDATA=dict()
        # 計算目前實際取得數據筆數（後續改成以抓取數量來終止爬蟲）
        self.TOSTORENUMS=0
        # 用來判斷是否只顯示單筆結果！
        self.debug=True

    # 每次crawler信義房屋流程開始
    def qRowsToJson(self, start=0):
        # 信義房屋固定參數
        perPageRows=30
        filtertype='publish-desc'
        page=1

        # 暫時直接配置每次抓取次數（頁數）
        if self.debug:
            preTimes=1
        else:
            preTimes=10

        resp_session = requests.Session()
        cookiedict=resp_session.cookies.get_dict()
        headers_set=[
            {
                'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
            },
            {
                'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:53.0) Gecko/20100101 Firefox/53.0'
            },
            {
                'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4'
            }
        ]
        sinyi_headers=headers_set[randint(0, 2)]

        headerdict={
            'Host': 'buy.sinyi.com.tw',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Referer': 'http://buy.sinyi.com.tw/list/index.html',
            'X-Requested-With': 'XMLHttpRequest',
            'Connection': 'keep-alive',
            'Cache-Control': 'max-age=0',
            'Content-Length': '0',
        }
        # resp_session.headers.update(sinyi_headers)
        # x = resp_session.post(
        #       'http://buy.sinyi.com.tw/cgi/member/isLogin.json',
        #       headers=headerdict
        #     )
        # cookiedict=resp_session.cookies.get_dict()
        # 為了第二次傳送處理！
        str_cookie=': '.join(['%s=%s' % (k, v) for (k, v) in cookiedict.items()])
        # print(str_cookie)
        headerdict['Cookie']=str_cookie

        bw_cookies = {
            'Connection': 'keep-alive',
            'Content-Length': '411',
            'Origin': 'http://buy.sinyi.com.tw',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': 'http://buy.sinyi.com.tw/list/index.html',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ja;q=0.2',
            'Cookie': '__uid=wKgIaVksSNkopy5DBla+Ag==; incap_ses_570_985953=6ExsO7BxUC103pTBOQzpB9RILFkAAAAADXtW1M4GlmxasHG7RvuHPQ==; visid_incap_1001522=Tp/PRHoUS+2fFzMj1zSmsdlILFkAAAAAQUIPAAAAAACCgKv+ikwoIt36xwiO/W0J; incap_ses_453_1001522=2lLAD1SFdSUmEphzkGFJBtlILFkAAAAAXmQKdQfUPywkLZ0zZEVXzw==; __BWfp=c1496074457664xbc59aa7cf; visid_incap_985953=hhFvxUd3Qru7DFnOWzS9dtRILFkAAAAAQUIPAAAAAADQpnmH5qeRCqKl0XmXlEs6; incap_ses_625_985953=0A8aY87SRQiENItAU3KsCGmLLVkAAAAAS0w3gUjqiooaQfJWns4AIg==; incap_ses_459_1001522=cPGUXioTXQ2UVCUCf7JeBm2LLVkAAAAAMGQDJrdCiUjRf26a2s5X2A==; _gat_UA-12807964-17=1; _ga=GA1.3.1060180085.1496074457; _gid=GA1.3.481316281.1496157085; _ga=GA1.4.1060180085.1496074457; _gid=GA1.4.1473699523.1496157095; _gat=1            ',
        }
        sinyi_params = {
            "params":"Taipei-city",
            "page":"1",
            "limit":"30",
            "returnParams":"NO,name,description,address,areaLand,areaBuilding,areaBuildingMain,areaBalcony,price,priceFirst,discount,type,use,room,hall,bathroom,openroom,roomplus,hallplus,bathroomplus,openroomplus,age,floor,inc,imgCount,imgDefault,bigImg,staffpick,decoar,pingratesup,community,lift,parking,customize,keyword,zipcode"
        }

        for i in range(0, preTimes):
            resp = resp_session.post(
                    self.crawlURL,
                    params=sinyi_params,
                    cookies=bw_cookies,
                    headers={
                        'Cache-Control':'max-age=0',
                        'Connection':'keep-alive',
                        'Content-type':'application/x-www-form-urlencoded',
                        'Host':'buy.sinyi.com.tw',
                        'Origin':'http://buy.sinyi.com.tw',
                        'Referer':'http://buy.sinyi.com.tw/list/publish-desc/index.html'
                      }
                   )

            try:
                Json=resp.json()
                # pprint(sinyi_params)
            # @todo : 判斷沒有正確取得Json內容時的處理流程！
            except ValueError as e:
                print('無法抓取信義房屋資料錯誤處理部份，判斷回應內容來改變對應機制！')
                print(self.crawlURL)
                # 像信義請求cookie值
                # pprint(resp.cookies)
                return

            # 數據總筆數 / 分頁總數
            self.REPARAMS={
                'TOTAL_CRAWLDATA':Json['OPT']['total'],
                'TOTAL_PAGE':Json['OPT']['totalPage']
            }
            # 數據資料（本身已經就是dict結構！可直接處理）
            """
            {
                NO : "99498Y"
                zipcode : 106
                address : "台北市大安區市民大道三段"
                age : 0
                areaBalcony : 7.61
                areaBuilding : 96.7
                areaBuildingMain : 49.31
                areaLand : null
                bathroom : 3
                bathroomplus : 0
                bigImg : ["http://album.s3.hicloud.net.tw/99498Y/A.JPG?t=1496074507"]
                imgDefault : "http://thumb.s3.hicloud.net.tw/99498Y/A.JPG?t=1496074507"
                communitys : []
                customize : null
                decoar : null
                description : ["知名建商指標建案", "北市核二千坪大基地", "大面寬棟距遠邊間採光", "日式精工現代文藝設計"]
                hall : 2
                hallplus : 0
                room : 4
                roomplus : 0
                parking : "坡道平面"
                type : "大樓、預售"
                use : "其他"

                floor : "9/23"
                inc : 2
                lift : 3
                name : "明日博Ｃ１－９樓"
                openroom : 0
                openroomplus : 0
                pingratesup : null
                price : 9900
                priceFirst : null
                price_item : "此物件含車位，計算方式請洽業務"
                staffpick : null
                imgCount : 9
                discount : null
            }
            """
            self.ORIGIONJSON=Json['OPT']['List']
            pprint(self.ORIGIONJSON)
            print('get json fine!')


            # single item json
            oneitem=requests.post(
                'http://buy.sinyi.com.tw/house/'+Json['OPT']['List'][0]['NO']+'.html',
                params=sinyi_params,
                cookies=cookiedict,
                headers={
                    'Cache-Control':'max-age=0',
                    'Connection':'keep-alive',
                    'Content-type':'application/x-www-form-urlencoded',
                    'Host':'buy.sinyi.com.tw',
                    'Origin':'http://buy.sinyi.com.tw',
                    'Referer':'http://buy.sinyi.com.tw/list/publish-desc/index.html'
                  }
            )
            pprint(oneitem.raw)
            return

            if not self.debug:
                self.parser("Json")
            else:
                pprint(self.ORIGIONJSON)
        pass


    """
    act1. 直接產生soup解析物件
    act2. 組成物件變數（dist型態）
    act3. 決定導出json or mysql
    """
    def parser(self,output='Json'):
        # 取得郵遞區號對照表
        fp = open(os.path.dirname(os.path.abspath(__file__))+'/zipcode.json','r', encoding='utf8')
        ZIPCODE = json.loads(fp.read())

        # 直接解析Json裡面的欄位！
        for item in self.ORIGIONJSON:
            pprint(item)
            # 地址欄位
            self.ParserData["addressSegment"]=item['address']
            # 屋齡
            self.ParserData["houseage"]=item['age']
            # 坪數(權狀坪數、不含車位坪數、車位坪數)
            self.ParserData['area']=float(item['areaBuilding'])
            # 有無車位(可進一步解析類型、有無)
            if item['parking']=='無車位':
                self.ParserData["parking"]=0
                self.ParserData['area_parking']=0
            else:
                self.ParserData["parking"]=1
                self.ParserData['area_woparking']=float(item['areaBuildingMain'])+float(item['areaBalcony'])
                self.ParserData['area_parking']=float(item['areaBuilding'])-self.ParserData['area_woparking']
            # 格局
            self.ParserData['patterns_room'] = item['room']
            if int(item['room']) > 0:
                ps.append(item['room']+'房')
            self.ParserData['patterns_hall'] = item['hall']
            if int(item['hall']) > 0:
                ps.append(item['hall']+'廳')
            self.ParserData['patterns_bathroom'] = item['bathroom']
            if int(item['bathroom']) > 0:
                ps.append(item['bathroom']+'衛')
            self.ParserData['patterns_balcony'] = item['balcony']
            if int(item['balcony']) > 0:
                ps.append(item['balcony']+'陽台')
            # 組合格局字串
            self.ParserData['patterns']=''.join(ps)
            # 主要圖片
            self.ParserData["cover_src"]=item['bigImg'][0]
            # 社區
            if len(item['community']) > 0:
                self.ParserData["community"]=item['community'][0]
            # 根據郵遞區號來判斷
            (azip for azip in ZIPCODE if azip["ZipCode"] == item["zipcode"]).next()
            # 城市
            self.ParserData["city"]=azip['City']
            # 行政區
            self.ParserData["district"]=azip['Area']
            # 排除表情字元
            self.ParserData["title"]= remove_emoji(item['name'])
            # 用途
            self.ParserData["usefor"]=item['use']
            # 型態
            t=item['type'].split('、')
            if t[0]=='電梯華廈':
                t[0]='電梯大樓'
            self.ParserData["type"]=t[0]
            # 總價
            self.ParserData["price_total"]=item['price']

            pprint(self.ParserData)
            break

            if not self.ParserData:
                print("單筆記錄解析資料錯誤，終止程式!")
                sys.exit(1)
            elif self.debug:
                pprint(self.ParserData)
                self.CRAWLDATA.append(self.ParserData)
                break
            else:
                self.CRAWLDATA.append(self.ParserData)
                self.ParserData=dict()
            # 物件編號（包含物件內容頁網址）
            self.parserItemPage(item['NO'])

        # pprint(self.CRAWLDATA)
        sys.exit()
        if not self.CRAWLDATA:
            print("集合資料解析資料錯誤，終止程式!")
        elif self.debug:
            func = getattr(self, "writeTo"+output)
            func()
            print("已將爬蟲結果寫入output!")
        elif self.hasData:
            print("持續擷取下一頁，暫存本業結果寫入CRAWLDATA")
        else:
            func = getattr(self, "writeTo"+output)
            func()
            print("已將爬蟲結果寫入output!")
        return 1


    """
        解析物件內容頁面欄位
    """
    def parserItemPage(self, NO):
        # 從本機下載檔案進行解析測試！
        if self.assignSource=='local':
            with open(os.path.dirname(os.path.abspath(__file__))+'/sample/sinyi_detail.htm','r') as resp:
                soup = BeautifulSoup(resp.read(),"html.parser")
            print("開始從本機檔案直接進行擷取")
        elif self.assignSource=='remote':
            self.ParserData["url_houseitem"]='http://buy.sinyi.com.tw/house/'+NO+'.html'
            resp = requests.get(toOldURL, cookies={})
            soup = BeautifulSoup(resp.text,"html.parser")
            print("開始從指定網址直接進行擷取")

        # pprint(cover.get('src'))
        dinfo=soup.find("ul",{"id":"attr"})

        # 單價
        if len(dinfo.findAll("li")) > 1:
            singleprice=float(Decimal(sub(r'[^\d.]', '', dinfo.findAll("li")[1].findAll("span")[1].find("em").text)))
            self.ParserData["princ_single"]=singleprice

        # 型態/現況 dinfo.findAll("li")[6].findAll("span")[1].text
        STAT=dinfo.findAll(text='型態/現況：')
        if len(STAT) is not 0:
            stattmp=STAT[0].parent.parent.findAll("span")[1].text
            # pprint(stattmp)
            stats=stattmp.split('/')
            # pprint(stats)
            housetype=stats[0]
            self.ParserData["house_type"]=housetype
            if len(stats) > 1:
                houseusefor=stats[1]
                self.ParserData["house_usefor"]=houseusefor

        # 樓層 Fs=dinfo.findAll("li")[4].findAll("span")[1].text
        Fs=dinfo.findAll(text='樓層：')
        if len(Fs) is not 0:
            floors=Fs[0].parent.parent.findAll("span")[1].text.split('/')
            atfloor=floors[0].strip('F')
            self.ParserData["floor_at"]=atfloor
            # 樓層(floor_at=整棟，型態:廠辦無設定！)
            if self.ParserData['floor_at']=='整棟' and self.ParserData['house_type']=='':
                if re.search('[土|農|工業用]地',self.ParserData['title']) or self.ParserData['usefor']=='土地':
                    self.ParserData['house_type']=='土地'
                elif self.ParserData['usefor']=='廠房':
                    self.ParserData['house_type']=='廠辦'

            if len(floors) > 1:
                totalfloor=floors[1].strip('F')
                self.ParserData["floor_total"]=totalfloor

        # 地圖網址
        # 街景模式網址
        mstreeturl=soup.find("div",{"id":"streetRound"}).find("iframe").get('src')
        self.ParserData["url_streetmap"]=self.crawlURL+mstreeturl
        # 一般模式網址
        murl=soup.find("div",{"id":"mapRound"}).find("iframe").get('src')
        if murl:
            mapurl=murl.replace('detail=detail','s=j_edit_maps')
            self.parserMapPage(self.crawlURL+mapurl)
        pass

    """
        解析物件內容頁面導向的地圖頁面欄位
    """
    def parserMapPage(self, toMapUrl):
        self.ParserData["url_housemap"]=toMapUrl
        resp = requests.get(toMapUrl, cookies={"housenrich": "1"})
        soup = BeautifulSoup(resp.text,"html.parser")
        # with open(os.path.dirname(os.path.abspath(__file__))+'/sample/sinyi_map.htm','r') as resp:
        #     soup = BeautifulSoup(resp.read(),"html.parser")

        # 物件經度
        lat=soup.find("input",{"id":"lat"}).get('value')
        self.ParserData["lat"]=lat
        # 物件緯度
        lng=soup.find("input",{"id":"lng"}).get('value')
        self.ParserData["lng"]=lng
        pass


if __name__ == "__main__":

    # HD = {
    #     'Host': 'buy.cthouse.com.tw',
    #     'Connection': 'keep-alive',
    #     'Upgrade-Insecure-Requests': '1',
    #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
    #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    #     'Referer': 'http://www.cthouse.com.tw/',
    #     'Accept-Encoding': 'gzip, deflate, sdch',
    #     'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ja;q=0.2',
    #     'Cookie': 'ASP.NET_SessionId=ppo2bwa1ywbthklam1tmnwgf; __sonar=5216059516396497969; uuid=32b09f4e-fef6-43ac-b594-c63e968a858f; ROUTEID.4244c492077fb418d435906f3886a321=.node1; __BWfp=c1496137193170x0e36e198c; search=%7b%22arg%22%3a%22%e8%87%ba%e5%8c%97%e5%b8%82-city%2f%22%7d; _ga=GA1.4.1923222864.1496137183; _gid=GA1.4.1184672206.1496137206; _TUCI=sessionNumber+1000&ECId+100&hostname+buy.cthouse.com.tw&pageView+2000; _TUCI_T=sessionNumber+17316&pageView+17316; __asc=244cedab15c5950a2f6490b4bfb; __auc=b93cfb3015c58b9ad8bbc640509; _gat_UA-10600360-1=1; _ga=GA1.3.1923222864.1496137183; _gid=GA1.3.2034115555.1496147076; _dc_gtm_UA-34980571-15=1',
    # }
    # ctres=requests.get(
    #             'http://buy.sinyi.com.tw/cgi/search/listSearch.json',
    #             headers=HD
    #         )
    # pprint(ctres.text)
    # sys.exit(0)
    # soup = BeautifulSoup(ctres.text,"html.parser")
    # print("開始從指定網址直接進行擷取")
    mysql = writetosinyi()
    for item in mysql.getAllTabs('sinyi'):
        cr=crawlsinyi(item,'remote')
        # cr.debug=False
        cr.qRowsToJson()
        # cr.parser('Json')
        # cr.writeToJson()
        # mysql.distToRaw('crawldata_sinyi', cr.CRAWLDATA)
        # mysql.updateParams(item['tabsId'], self.REPARAMS)
    sys.exit(0)

# -*- coding: UTF-8 -*-
import os
import json
import sys
# python 處理Email格式時間轉換timestamp所需函式庫
import time
import MySQLdb
from base import BasicDatabase
from phpserialize import *
from pprint import pprint


class writeto591(BasicDatabase):
    """docstring for writeto591"""
    def __init__(self):
        # 若要額外指定設定檔位置！
        jsonconf=None
        # @todo : 根據不同來源應有不同判斷方式，故要改成繼承class後覆寫function型式！
        super().__init__(jsonconf)
        self.CRAWLDICT=list()
        self.RECRAWL=list()


    """
    寫入資料前判斷該筆資料是否已存在
    1. 根據傳入要比對的欄位名稱從對應紀錄內取出資料值
    3. 比對欄位異動內容，
    4. 處理欄位filterType設定
    """
    def checkifCrawldata(self, orgCRAWL, matchList=None):
        try:
            filters=list()
            assignto_table='crawldata_591'
            # 指定比對欄位list
            if matchList is None:
                matchList=['title','url_houseitem']

            for key,crawdata in orgCRAWL.items():
                filters=list()
                mapdict=dict()
                # print('索引:'+str(key))
                # pprint(crawdata.keys())
                for fname in matchList:
                    if fname in crawdata.keys():
                        if type(crawdata[fname]) is int:
                            mapdict[fname]=int(crawdata[fname])
                            fmstr='%('+fname+')d'
                        # elif type(crawdata[fname]) is float:
                        #     mapdict[fname]=float(crawdata[fname])
                        #     fmstr='%('+fname+')i'
                        else:
                            mapdict[fname]=crawdata[fname]
                            fmstr='%('+fname+')s'
                        # age = %(age)s
                        filters.append("`"+fname+"` = "+fmstr)
                sql="SELECT * FROM %s WHERE %s " % (assignto_table, " AND ".join(filters))
                self.CURSOR.execute(sql, mapdict)
                # print(sql)
                # pprint(mapdict)
                # result is dictory
                row = self.CURSOR.fetchone()
                # 將該筆資料從self.CRAWLDICT移除，將入self.RECRAWLS
                # pprint(row['title'])
                if row:
                    self.RECRAWL.append(crawdata)
                else:
                    self.CRAWLDICT.append(crawdata)
                # # remove
                # orgCRAWL.pop(key)

            # pprint(len(self.CRAWLDICT))
            # pprint(self.RECRAWLS)
            # sys.exit()
        except MySQLdb.ProgrammingError as e:
            print(e)
            sys.exit(1)
        except MySQLdb.Error as e:
            print("[checkifCrawldata]Error %d: %s " % (e.args[0], e.args[1]))
            print('SQL語法: ',sql)
            sys.exit(1)

    """
    轉換從解析抓取儲存的結構成MySQL內配置欄位名稱，並進行部分欄位判斷處理
    """
    def mappingFields(self, crawldata):
        dbCrawl=dict()

        for key, val in crawldata.items():
            if key=='title':
                dbCrawl['title']=val
            if key=='url_houseitem' and val:
                dbCrawl['sourcelink']=val
            if key=='cover_src' and val:
                dbCrawl['cover']=val
            if key=='city' and val:
                dbCrawl['addressCity']=val
            if key=='district' and val:
                dbCrawl['addressDistrict']=val
            if key=='addressSegment' and val:
                dbCrawl['addressSegment']=val
            if key=='price_total' and val:
                dbCrawl['priceTotal']=val
            if key=='house_type' and val:
                dbCrawl['type']=val
            if key=='house_usefor' and val:
                dbCrawl['usefor']=val
            if key=='patterns_room' and val:
                dbCrawl['patternRoom']=val
            if key=='patterns_hall' and val:
                dbCrawl['patternHall']=val
            if key=='patterns_bathroom' and val:
                dbCrawl['patternBathroom']=val
            if key=='patterns_balcony' and val:
                dbCrawl['patternBalcony']=val
            if key=='floor_at':
                if val=='整棟':
                    dbCrawl['floorAt']=0
                elif val=='B1':
                    dbCrawl['floorAt']=-1
                else:
                    dbCrawl['floorAt']=int(val)
            if key=='floor_total' and val:
                dbCrawl['floorTotal']=val
            if key=='houseage_count' and val:
                dbCrawl['houseage']=val
            if key=='parking' :
                if val=='無':
                    dbCrawl['parking']=0
                else:
                    dbCrawl['parking']=1
            if key=='area' and val:
                dbCrawl['areaTotal']=val
                if 'area_woparking' in crawldata:
                    dbCrawl['areaMain']= crawldata['area_woparking']
                else:
                    dbCrawl['areaMain']=val
            if key=='area_parking' and val:
                dbCrawl['areaParking']=val
            if key=='lat' and val:
                dbCrawl['lat']="{0:.6f}".format(val)
            if key=='lng' and val:
                dbCrawl['lng']="{0:.6f}".format(val)
            if key=='url_streetmap' and val:
                dbCrawl['sourceMap']=val
            if key=='community' and val:
                dbCrawl['community']=val

        # 額外附加initTime欄位！
        dbCrawl['initTime']=int(time.time())

        pprint(dbCrawl)
        return dbCrawl


    """
    處理寫入原始數據表crawldata_591
    1. 取得單一數據資料結構
    2. 配置columns_string內容
    3. 配置crawldata欄位list根據loop產生寫入語法
    4. 自動判斷單一數據對應list欄位，若不相符則排除
    """
    def distToRaw(self, assignto_table='crawldata_591'):
        try:
            instId=list()
            # 取得表格欄位名稱清單
            opt={"output":False,"exKey":1,"key":'crawldataId'}
            fieldNames = self.getTableColumnNames(assignto_table, opt)

            # 使用特定語法直接將所有取得的物件寫入資料庫！
            for crda in self.CRAWLDICT:
                # 根據fieldNames索引位置重組value_string
                vlist=list()
                tmpFields = list(fieldNames)
                for fname in fieldNames:
                    if fname in crda.keys():
                        vlist.append(str(crda[fname]))
                    else:
                        tmpFields.remove(fname)
                # 組合成字串表示
                columns_string = '(`'+'`,`'.join(tmpFields)+'`)'
                values_string = "('"+"','".join(vlist)+"')"

                if not len(tmpFields)==len(vlist):
                    print("欄位數量與數據記錄個數不符！")
                else:
                    sql = """INSERT INTO %s %s
                             VALUES %s"""%(assignto_table, columns_string, values_string)
                    # print(sql)
                    # sys.exit()
                    self.CURSOR.execute(sql)
                    instId.append(self.CURSOR.lastrowid)
            # 回除建立數據主鍵集合
            return instId
        except MySQLdb.Error as e:
            print("[distToRaw]Error accure %d: %s " % (e.args[0], e.args[1]))
            print('SQL語法: ',sql)
            sys.exit(1)


    """
    處理寫入數據記錄表
    1. 寫入前檢查該筆數據是否存在 call self.checkifCrawldata
    2. 使用params方式組合Insert語法
    """
    def toCrawldata(self):
        try:
            instId=list()
            assignto_table='crawldata'
            # 取得表格欄位名稱清單
            opt={"output":False,"exKey":1,"key":'crawldataId'}
            fieldNames = self.getTableColumnNames(assignto_table, opt)

            # 使用特定語法直接將所有取得的物件寫入資料庫！
            for crda2 in self.CRAWLDICT:
                # 轉換欄位
                onecrawldata=self.mappingFields(crda2)

                # 根據fieldNames索引位置重組value_string
                vlist=list()
                tmpFields = list(fieldNames)
                for fname in fieldNames:
                    if fname in onecrawldata.keys():
                        vlist.append(str(onecrawldata[fname]))
                    else:
                        tmpFields.remove(fname)
                # 組合成字串表示
                columns_string = '(`'+'`,`'.join(tmpFields)+'`)'
                # 根據欄位數轉成組合%s字串
                valuenums_string = '('+'%s,' * len(tmpFields)
                valuenums_string = valuenums_string[:-1]+')'

                values_string = "('"+"','".join(vlist)+"')"

                if not len(tmpFields)==len(vlist):
                    print("欄位數量與數據記錄個數不符！")
                else:
                    # sql = """INSERT INTO %s %s
                    #          VALUES %s"""%(assignto_table, columns_string, values_string)
                    sql = ('INSERT INTO %s %s '
                           'VALUES %s '
                           %(assignto_table, columns_string, valuenums_string))
                    self.CURSOR.execute(sql, vlist)
                    self.CrawldataId=self.CURSOR.lastrowid

                    # 繼續寫入onetoCrawldataExtend
                    # self.onetoCrawldataExtend()

                    # stage4(5) 再處理！附加延伸欄位內容
                    # self.onetoCrawldataExtend(self.CURSOR.lastrowid, onecrawldata)
                    instId.append(self.CURSOR.lastrowid)
            # 回除建立數據主鍵集合
            return instId
        except MySQLdb.Error as e:
            print("[toCrawdata]Error %d: %s " % (e.args[0], e.args[1]))
            print('SQL語法: ',sql)
            sys.exit(1)


    """
    處理寫入數據延伸欄位記錄表crawldata_extendinfo
    1. visited(optional)
    2. 更新時間
    """
    def onetoCrawldataExtend(self, crawldataId, onecrawldata):
        try:
            assignto_table='crawldata_extendinfo'

            opt={"output":False,"exKey":1,"key":'cextendinfoId'}
            fieldNames = self.getTableColumnNames(assignto_table, opt)

            #act1. 指定key欄位！

            #act2. 根據該key確認之前的數值是否有異動

        except MySQLdb.Error as e:
            print("[onetoCrawldataExtend]Error %d: %s " % (e.args[0], e.args[1]))
            print('SQL語法: ',sql)
            sys.exit(1)

    """
    處理
    """
    def updateCrawllimit(self, tname, ntRownums):
        try:
            sql = "UPDATE tabs SET crawllimit=%d WHERE type='%s'" % (ntRownums, tname)
            self.CURSOR.execute(sql)
        except MySQLdb.Error as e:
            print("[updateCrawllimit]Error %d: %s " % (e.args[0], e.args[1]))
            print('SQL語法: ',sql)
            sys.exit(1)


class writetosinyi(BasicDatabase):
    """docstring for writetosinyi"""
    def __init__(self):
        # 若要額外指定設定檔位置！
        jsonconf=None
        # @todo : 根據不同來源應有不同判斷方式，故要改成繼承class後覆寫function型式！
        super().__init__(jsonconf)
        self.CRAWLDICT=list()
        self.RECRAWL=list()



if __name__ == "__main__":
    mysqldb = writeto591()
    # 直接配置主要Crawldict到mysqldb內
    # mysqldb.CRAWLDICT = xx
    fp = open('/website/crawler/scripts/pk_crawler/591/temp.json', 'r', encoding='utf8')
    CDICT = json.loads(fp.read())
    mysqldb.checkifCrawldata(CDICT)
    # 處理完後直接使用 self.CRAWLDICT 與 self.RECRAWLS
    mysqldb.distToRaw('crawldata_591')
    mysqldb.toCrawldata()
    mysqldb.close()

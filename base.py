# -*- coding: UTF-8 -*-
"""
@author San Huang
建立爬蟲函式庫基礎類別
"""
import io
import re
import os
import sys
import json
# python 處理Email格式時間轉換timestamp所需函式庫
import time
import shutil
from os import remove, close
from tempfile import mkstemp
# python 處理Email格式時間轉換timestamp所需函式庫
from email.utils import parsedate_tz, mktime_tz, formatdate
from shutil import move
from time import sleep,strftime,strptime
from pprint import pprint
from bs4 import BeautifulSoup
import MySQLdb
from phpserialize import *

class Crawler(object):
    """docstring for Crawler"""
    def __init__(self, item):
        # 直接寫入mysql處理使用
        self.tabsId = item['tabsId']
        # 作為Json儲存目錄使用
        self.storeJsondir = item['dirname']
        # 指定預設爬蟲網址
        self.crawlURL = item['source']
        # 指定爬蟲解析後轉存成單一個dist變數
        self.ParserData = dict()
        # 將個別ParserData存放至整個爬蟲集合變數
        self.CRAWLDATA = list()


    """
    act1. 直接產生soup解析物件
    act2. 組成物件變數（dist型態）
    act3. 決定導出json or mysql
    """
    def parser(self,output='Json'):
        # self.ParserData = [{"a":1,"b":2},{"a":3,"b":4}]

        # 處理解析部分！

        if not self.ParserData:
            print("解析資料錯誤，終止程式!")
        else:
            func = getattr(self, "writeTo"+output)
            func()
        return 1

    """
    產生寫入檔案
    將產生物件轉成json
    """
    def writeToJson(self):
        with io.open(self.setStoreFilePath(), 'w', encoding='utf-8') as jf:
            jf.write(json.dumps(self.CRAWLDATA, indent=4, ensure_ascii=False))
        # with open(self.setStoreFilePath(), 'w') as tofile:
        #     json.dump(json.dumps(self.CRAWLDATA, indent=4, ensure_ascii=False) , tofile)
        pass

    # 若導出json呼叫配置json檔案用(writetojson呼叫！)
    def setStoreFilePath(self, prepath=None, filename=None):
        if not prepath:
            prepath=os.path.dirname(os.path.abspath(__file__))
        if not filename:
            filename=strftime("%m-%d-%H%M.json")

        targetdir = prepath+'/'+self.storeJsondir
        if not os.path.exists(targetdir):
            os.makedirs(targetdir)
            os.chmod(targetdir,0o777)

        targetfile = targetdir+'/'+filename
        print(targetfile)
        return targetfile


    # act1. 取得完整單筆數據內容結構 json_data
    def writeToMysql(self, json_data):
        return 1

    def replace(self,pattern, subst):
        fh, abs_path = mkstemp()
        with open(abs_path,'w') as new_file:
            with open(self.targetfile) as old_file:
                for line in old_file:
                    new_file.write(line.replace(pattern, subst))
        close(fh)
        remove(self.targetfile)
        move(abs_path, self.targetfile)


    def _log(self, msg):
        # return
        print(strftime("[%H:%M:%S]")+msg)

    # 當呼叫mysql形式時會將暫存Json檔目錄移除
    def delete(self):
        if os.path.exists(self.targetdir):
            shutil.rmtree(self.targetdir)
        return


class BasicDatabase(object):
    """docstring for BasicDatabase"""
    def __init__(self,config_file=None):
        # 根據json設定檔配置資料庫
        if config_file is None:
            config_file = os.path.dirname(os.path.abspath(__file__))+'/config.json'
        with open(config_file, encoding='utf-8') as jfile:
            conf = json.loads(jfile.read())

        # 建立連線
        self.DBObject = MySQLdb.Connection(conf['dbcon']['host'], conf['dbcon']['user'], conf['dbcon']['passwd'], conf['dbcon']['db'], charset='utf8')
        self.DBObject.autocommit(True)
        self._buildCursor()
        # 設定實際處理還是測試單次流程
        self.debug=False


    def _buildCursor(self, useDict=1):
        # 從連線資源存取指標物件(指定fetch取得物件使用Dict類型儲存key=value！)
        # http://www.dahuatu.com/9jWbXgVBmx.html
        if useDict == 1:
            self.CURSOR = self.DBObject.cursor(MySQLdb.cursors.DictCursor)
        else:
            self.CURSOR = self.DBObject.cursor()
        # 執行指令
        # cur.execute('SET NAMES utf8')


    # 直接取得頁籤項目回傳
    def getAllTabs(self, settype=''):
        if not settype:
            print("未指定類型代碼，終止程式")
            print()
            sys.exit(1)

        try:
            sql = "SELECT * FROM tabs WHERE type='%s' GROUP BY dirname" % (settype)
            self.CURSOR.execute(sql)
            # fetchone / fetchall
            self.tabRows = self.CURSOR.fetchall()
            return self.tabRows

        except MySQLdb.Error as e:
            print("Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)


    """
        傳入欄位list組成資料結構
        表格動態化 tableName
    """
    def getTableColumnNames(self, tableName, optDists={"output":False,"exKey":False,"key":None}):
        try:
            # 取得表格欄位名稱清單
            self.CURSOR.execute("SELECT * FROM "+tableName+" LIMIT 1")
            colList = [description[0] for description in self.CURSOR.description]
            # 排除主鍵欄位情況
            if optDists["exKey"] and optDists["key"] in colList:
                colList.remove(optDists["key"])

            if optDists["output"]:
                pprint(colList)
            else:
                return colList
        except MySQLdb.Error as e:
            print('SQL語法: ',sql)
            print("[BasicDatabase.getTableColumnNames]Error accure %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)


    """
    寫入原始數據表基本範本
    """
    def distToRaw(self, distDatas=None):
        try:
            # 使用特定語法直接將所有取得的物件寫入資料庫！
            for myDict in distDatas:
                # columns_string= "('"+"','".join(myDict.keys())+"')"
                columns_string= '('+','.join(myDict.keys())+')'
                # values_string = '('+','.join(map(str,myDict.values()))+')'
                values_string = "('"+"','".join(map(str,myDict.values()))+"')"

                sql = """INSERT INTO %s %s
                     VALUES %s"""%(tableName, columns_string, values_string)
                # print(sql)
                self.cur.execute(sql)
                # 產生的主鍵
                instId=self.cur.lastrowid
        except MySQLdb.Error as e:
            print("[distToRaw]Error accure %d: %s " % (e.args[0], e.args[1]))
            print('SQL語法: ',sql)
            sys.exit(1)


    """
    """
    def updateFirstrow(self, tabsId, firstRow):
        try:
            sql = "UPDATE `tabs` SET `crawllimit`=%s WHERE `tabsId`=%s" % (firstRow,tabsId)
            self.CURSOR.execute(sql)
            # for item in res:
            #     print(item['label'])
        except MySQLdb.Error as e:
            print("Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)
        return 1

    """
    """
    def close(self):
        if self.DBObject:
            self.CURSOR.close()
            self.DBObject.close()
        return 1


if __name__ == "__main__":
    c=Crawler()
    sys.exit(0)

# -*- coding: UTF-8 -*-
import os
import pprint
import json
import shutil
import calendar
from time import strftime
import dateutil.parser
import facepy
from .mydb import mydb

class Crawler:
    def __init__(self, fbitem):
        # 直接寫入mysql處理使用
        self.tabsId = fbitem['tabsId']
        # 作為Json儲存目錄使用
        self.name = fbitem['dirname']
        APP_ID = '1686307191627331'
        APP_SECRET = 'b2e0507d05172d3312f7f8785b9dc5fd'
        # get App Token from pkrobot app
        app_token = facepy.utils.get_application_access_token(APP_ID, APP_SECRET)
        graph = facepy.GraphAPI(app_token)
        self.exceptflag=0
        try:
            # get lastest posts in json format from graph api with extend fields
            response = graph.get(self.name+'/posts?fields=id,message,created_time,link,type,description,picture&limit=4')
            if( response['data'] ):
                self.json_posts = response['data']
        except facepy.exceptions.OAuthError as e:
            self.exceptflag=1
            pass


    def setStoreFilePath(self, prepath):
        self.targetdir = prepath+'/'+self.name
        if not os.path.exists(self.targetdir):
            os.makedirs(self.targetdir)
            os.chmod(self.targetdir,0o777)
        return 1

    def parser(self, fb_post):
        x = dateutil.parser.parse(fb_post['created_time'])
        timestamp = calendar.timegm(x.timetuple())
        if( 'message' in fb_post ):
            # 過濾\n
            title=fb_post['message'].rstrip()
        else:
            title='無原始標題資訊'
        if( 'description' in fb_post ):
            # 過濾\n
            content=fb_post['description'].rstrip()
        else:
            content=None
        if( 'link' in fb_post ):
            link=fb_post['link']
        else:
            link=None
        if( 'picture' in fb_post ):
            image=fb_post['picture']
        else:
            image=None

        return {"f_內文":content,"c_標題":title,"i_連結":link,"d_時間戳記":timestamp,"d_日期":fb_post['created_time'],"shareimage":image,"posttype":fb_post['type'],"postid":fb_post['id']}

    def writeToJson(self):
        if(self.exceptflag):
            return 0
        f = open(self.targetdir+'/'+strftime("%m-%d-%H%M.json"),'w')
        f.write('[')
        # json_datas=[]
        for post in self.json_posts:
            format_json = self.parser(post)
            # json_datas.append(format_json)
            jdata = json.dumps(format_json,ensure_ascii=False,indent=4,sort_keys=True)
            if( self.json_posts.index(post) != (len(self.json_posts)-1) ):
                jdata +=','
            f.write(jdata)
        f.write(']')
        f.close()
        # pp = pprint.PrettyPrinter(indent=2, compact=True)
        # pp.pprint(json_datas)
        return

    def writeToMysql(self):
        if(self.exceptflag):
            return 0
        mysql = mydb()
        for post in self.json_posts:
            format_json=self.parser(post)
            crawldataId=mysql.existCrawlerdata(format_json, 0)
            if( 'link' not in post ):
                show=post['message']
            else:
                show=post['link']
            if not crawldataId:
                # print("%s 未重複，新增crawldata: %d" % (show, crawldataId))
                # act3. 處理寫入紀錄，包含主紀錄與延伸紀錄
                crawldataId=mysql.insertCrawdata(format_json)
                mysql.insertTabCrawldata(crawldataId, self.tabsId, self.name)
                mysql.FBinsertCrawldataExtendinfo(crawldataId, format_json)
            # else:
                # cextendinfoId=mysql.FBupdateCrawldataExtendinfo(crawldataId, format_json)
                # print("%s 重複，更新延伸資訊" % (show))
        return 1

    # 當呼叫mysql形式時會將暫存Json檔目錄移除
    def delete(self, prepath):
        targetdir = prepath+'/'+self.name
        if os.path.exists(targetdir):
            shutil.rmtree(targetdir)
        return

if __name__ == "__main__":
    fb_prepath = os.path.realpath(os.path.dirname(os.path.abspath(__file__))+'/../../public/temp/FB')
    mysql = mydb()
    for item in mysql.getAllTabs(0):
        fbcrawler = Crawler(item)
        # 呼叫mycrawler進行parser處理
        fbcrawler.setStoreFilePath(fb_prepath)
        # fbcrawler.writeToJson()
        fbcrawler.writeToMysql()

    if( mysql.close() ):
        print('done!');

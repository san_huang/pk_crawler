import sys
# python 處理Email格式時間轉換timestamp所需函式庫
import time
import MySQLdb
from phpserialize import *

# test for connect to mysql imac_pkrobot
class mydb:
    def __init__(self):
        # self.host = '127.0.0.1'
        # self.user = 'sanslave'
        # self.passwd = 'omge0954'
        # self.db = 'imac_pkrobot'
        self.host = 'localhost'
        self.user = 'pkrobot_dbUser'
        self.passwd = '1q2w3e4r'
        self.db = 'pkrobot_phalcon'

        # 建立連線
        self.dbobj = MySQLdb.Connection(self.host, self.user, self.passwd, self.db, charset='utf8')
        self.dbobj.autocommit(True)
        self._buildCursor()

    def _buildCursor(self, useDict=1):
        # 從連線資源存取指標物件(指定fetch取得物件使用Dict類型儲存key=value！)
        # http://www.dahuatu.com/9jWbXgVBmx.html
        if useDict == 1:
            self.cur = self.dbobj.cursor(MySQLdb.cursors.DictCursor)
        else:
            self.cur = self.dbobj.cursor()
        # 執行指令
        # cur.execute('SET NAMES utf8')

    # 直接取得頁籤項目回傳
    def getAllTabs(self, settype):
        try:
            sql = "SELECT * FROM `tabs` WHERE `type`=%s GROUP BY `dirname`" % (settype)
            self.cur.execute(sql)
            # fetchone / fetchall
            res = self.cur.fetchall()
            return res
            # for item in res:
            #     print(item['label'])
        except MySQLdb.Error as e:
            print("Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)


    # 改用Facebook原文連結來判斷是否資料重複
    def existCrawlerdata(self, record, nowtype):
        try:
            if( not record['i_連結'] ):
            #     sql="SELECT * FROM `crawldata` WHERE `content`='%s'" % (record['f_內文'])
                sql="SELECT * FROM `crawldata` WHERE `title`=%s"
                self.cur.execute(sql, [record['c_標題']])
            # print(sql)
            else:
                sql="SELECT * FROM `crawldata` WHERE `sourcelink`=%s"
                self.cur.execute(sql, [record['i_連結']])
            # print(sql)
            res = self.cur.fetchall()
            if not res:
                return 0
            else:
                sql = "SELECT type FROM `tabs` WHERE tabsId = ( \
                        SELECT TabsId FROM `tabs_crawldata` WHERE `CrawldataId`=%d LIMIT 1)" % (res[0]['crawldataId'])
                # print(sql)
                self.cur.execute(sql)
                subres = self.cur.fetchall()
                if not subres:
                    return 0
                elif(nowtype == subres[0]['type']):
                    return res[0]['crawldataId']
                else:
                    return 0
        except MySQLdb.Error as e:
            print('SQL語法: ',sql)
            print("Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)


    def insertCrawdata(self, R):
        try:
            if not R['i_連結']:
                R['i_連結']=''
            sql = "INSERT INTO `crawldata` (`title`, `content`, `initTime`, `sourcelink`) \
                   VALUES (%s,%s,%s,%s)"
            self.cur.execute(sql, [R['c_標題'], R['f_內文'], R['d_時間戳記'], R['i_連結']])
            return self.cur.lastrowid
        except MySQLdb.Error as e:
            print('SQL語法: ',sql)
            print("FBinsertCrawdata Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)

    def insertTabCrawldata(self, crawldataId, tabsId, dirname):
        try:
            sql = "INSERT INTO `tabs_crawldata` (`TabsId`, `CrawldataId`, `Dirname`) \
                   VALUES (%s,%s,%s)"
            self.cur.execute(sql, [tabsId,crawldataId,dirname])
            return self.cur.lastrowid
        except MySQLdb.Error as e:
            print('SQL語法: ',sql)
            print("insertTabCrawldata Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)

    def FBinsertCrawldataExtendinfo(self, crawldataId, record):
        try:
            # shareimage
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) VALUES (%s,%s,%s,%s)"
            self.cur.execute(sql, [crawldataId, 'shareimage', record['shareimage'], int(time.time())])
            # type
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) VALUES (%s,%s,%s,%s)"
            self.cur.execute(sql, [crawldataId, 'type', record['posttype'], int(time.time())])
            # id(post at fb link)
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) VALUES (%s,%s,%s,%s)"
            self.cur.execute(sql, [crawldataId, 'postid', record['postid'], int(time.time())])
            return self.cur.lastrowid
        except MySQLdb.Error as e:
            print('SQL語法: ',sql)
            print("FBinsertCrawldataExtendinfo Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)

    def FBupdateCrawldataExtendinfo(self, crawldataId, record):
        try:
            # valuestr=dumps(record['shareimage']).decode("utf-8")
            sql = "SELECT `cextendinfoId` FROM `crawldata_extendinfo` \
                   WHERE `CrawldataId`=%s AND `key`='shareimage'" % (crawldataId)
            self.cur.execute(sql)
            res = self.cur.fetchall()
            if not res:
                sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                       VALUES (%d,'%s','%s',%d)" % (crawldataId, 'shareimage', record['shareimage'], int(time.time()) )
            else:
                sql = "UPDATE `crawldata_extendinfo` SET `value`='%s'\
                       WHERE `cextendinfoId`=%s" % (record['shareimage'],res[0]['cextendinfoId'])
            self.cur.execute(sql)
            return self.cur.lastrowid
        except MySQLdb.Error as e:
            print('SQL語法: ',sql)
            print("FBupdateCrawldataExtendinfo Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)
        except IndexError:
            pass

    def PTTinsertCrawldataExtendinfo(self, crawldataId, record):
        try:
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                   VALUES (%s,'%s','%s',%s)" % (crawldataId, '總數', record['h_推文總數']['all'], int(time.time()) )
            self.cur.execute(sql)
            # allid=self.cur.lastrowid
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                   VALUES (%s,'%s','%s',%s)" % (crawldataId, '一般', record['h_推文總數']['n'], int(time.time()) )
            self.cur.execute(sql)
            # allid=self.cur.lastrowid
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                   VALUES (%s,'%s','%s',%s)" % (crawldataId, '推文', record['h_推文總數']['g'], int(time.time()) )
            self.cur.execute(sql)
            # allid=self.cur.lastrowid
            sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                   VALUES (%s,'%s','%s',%s)" % (crawldataId, '噓文', record['h_推文總數']['b'], int(time.time()) )
            self.cur.execute(sql)
            return 1
        except MySQLdb.Error as e:
            print("PTTinsertCrawldataExtendinfo Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)

    def PTTupdateCrawldataExtendinfo(self, crawldataId, record):
        try:
            sql = "SELECT `cextendinfoId` FROM `crawldata_extendinfo` \
                   WHERE `CrawldataId`=%s AND `key`='總數'" % (crawldataId)
            self.cur.execute(sql)
            res = self.cur.fetchall()
            if res[0]['cextendinfoId']:
                sql = "UPDATE `crawldata_extendinfo` SET `value`='%s'\
                       WHERE `cextendinfoId`=%s" % (record['h_推文總數']['all'],res[0]['cextendinfoId'])
                self.cur.execute(sql)
                extendid=res[0]['cextendinfoId']
            else:
                sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                       VALUES (%s,'%s','%s',%s)" % (crawldataId, '總數', record['h_推文總數']['all'], int(time.time()) )
                self.cur.execute(sql)
                extendid=self.cur.lastrowid

            sql = "SELECT `cextendinfoId` FROM `crawldata_extendinfo` \
                   WHERE `CrawldataId`=%s AND `key`='一般'" % (crawldataId)
            self.cur.execute(sql)
            res = self.cur.fetchall()
            if res[0]['cextendinfoId']:
                sql = "UPDATE `crawldata_extendinfo` SET `value`='%s'\
                       WHERE `cextendinfoId`=%s" % (record['h_推文總數']['n'],res[0]['cextendinfoId'])
                self.cur.execute(sql)
                extendid=res[0]['cextendinfoId']
            else:
                sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                       VALUES (%s,'%s','%s',%s)" % (crawldataId, '一般', record['h_推文總數']['n'], int(time.time()) )
                self.cur.execute(sql)
                extendid=self.cur.lastrowid

            sql = "SELECT `cextendinfoId` FROM `crawldata_extendinfo` \
                   WHERE `CrawldataId`=%s AND `key`='推文'" % (crawldataId)
            self.cur.execute(sql)
            res = self.cur.fetchall()
            if res[0]['cextendinfoId']:
                sql = "UPDATE `crawldata_extendinfo` SET `value`='%s'\
                       WHERE `cextendinfoId`=%s" % (record['h_推文總數']['g'],res[0]['cextendinfoId'])
                self.cur.execute(sql)
                extendid=res[0]['cextendinfoId']
            else:
                sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                       VALUES (%s,'%s','%s',%s)" % (crawldataId, '推文', record['h_推文總數']['g'], int(time.time()) )
                self.cur.execute(sql)
                extendid=self.cur.lastrowid

            sql = "SELECT `cextendinfoId` FROM `crawldata_extendinfo` \
                   WHERE `CrawldataId`=%s AND `key`='噓文'" % (crawldataId)
            self.cur.execute(sql)
            res = self.cur.fetchall()
            if res[0]['cextendinfoId']:
                sql = "UPDATE `crawldata_extendinfo` SET `value`='%s'\
                       WHERE `cextendinfoId`=%s" % (record['h_推文總數']['b'],res[0]['cextendinfoId'])
                self.cur.execute(sql)
                extendid=res[0]['cextendinfoId']
            else:
                sql = "INSERT INTO `crawldata_extendinfo` (`CrawldataId`, `key`, `value`, `setTime`) \
                       VALUES (%s,'%s','%s',%s)" % (crawldataId, '噓文', record['h_推文總數']['b'], int(time.time()) )
                self.cur.execute(sql)
                extendid=self.cur.lastrowid
        except MySQLdb.Error as e:
            print("PTTupdateCrawldataExtendinfo Error %d: %s " % (e.args[0], e.args[1]))
            sys.exit(1)

    def close(self):
        if self.dbobj:
            self.cur.close()
            self.dbobj.close()
        return 1

if __name__ == "__main__":
    db = mydb()
    res=db.getAllTabs(0)
    print(res)
# -*- coding: UTF-8 -*-
import re
import os
import sys
import json
import requests
import shutil
from os import remove, close
from tempfile import mkstemp
# python 處理Email格式時間轉換timestamp所需函式庫
from email.utils import parsedate_tz, mktime_tz, formatdate
from shutil import move
from time import sleep,strftime,strptime
from bs4 import BeautifulSoup
from .mydb import mydb

class mycrawler:

	# def __init__(self, Boardname, Dir):
	def __init__(self, pttitem):
		# self.Storepath = os.path.join(Dir , strftime("%m-%d-%H%M.json"))
		# 直接寫入mysql處理使用
		self.tabsId = pttitem['tabsId']
        # 作為Json儲存目錄使用
		self.name = pttitem['dirname']
		self.URL = pttitem['source']

	def setStoreFilePath(self, prepath):
		self.targetdir = prepath+'/'+self.name
		if not os.path.exists(self.targetdir):
			os.makedirs(self.targetdir)
			os.chmod(self.targetdir,0o777)
		self.targetfile = self.targetdir+'/'+strftime("%m-%d-%H%M.json")
        # # html
        # self.htmldir = prepath+'/html'
        # if not os.path.exists(self.htmldir):
        #     os.makedirs(self.htmldir)
        #     os.chmod(self.htmldir,0o777)
        # # json
        # self.jsondir = prepath+'/json'
        # if not os.path.exists(self.jsondir):
        #     os.makedirs(self.jsondir)
        #     os.chmod(self.jsondir,0o777)
		return 1

	def parser(self,output='json',start=0,end=5):
		# page = start; times = end-start+1;
		g_id = 0;
		# for a in range(times):
			# self._log('存取第 '+ str(page)+' 頁面')
		# self._log('存取'+self.URL+" 最新頁面")
		# print('index is '+ str(page))
		resp = requests.get(
				url=self.URL,
				cookies={"over18": "1"}
			   )
		soup = BeautifulSoup(resp.text,"html.parser")

		if( output == 'json' ):
			self.sf=open(self.targetfile , "a")
			self.sf.write('[')
			for tag in soup.find_all("div","r-ent"):
				try:
					link = str(tag.find_all("a"))
					link = link.split("\"")
					link = "http://www.ptt.cc"+link[1]
					g_id = g_id+1

					d=self.parseGos(link,g_id)
					json_data=json.dumps(d,ensure_ascii=False,indent=4,sort_keys=True)+','
					self.sf.write(json_data)
				except:
				    pass
			self.sf.write(']')
			self.sf.close()
			self.replace(',]',']')
			os.chmod(self.targetfile,0o644)
		elif( output == 'mysql' ):
			for tag in soup.find_all("div","r-ent"):
				try:
					link = str(tag.find_all("a"))
					link = link.split("\"")
					link = "http://www.ptt.cc"+link[1]
					g_id = g_id+1

					json_data=self.parseGos(link,g_id)
					# 檢查是否推文-虛文> 30
					if((int(json_data['h_推文總數']['g'])-int(json_data['h_推文總數']['b'])) > 30):
						self.writToMysql(json_data)
				except:
				    pass
			# 移除整個目標目錄
			self.delete()
		sleep(0.1)
		# page += 1

	def writToMysql(self, json_data):
		mysql = mydb()
		crawldataId=mysql.existCrawlerdata(json_data, 1)
		if not crawldataId:
			# act3. 處理寫入紀錄，包含主紀錄與延伸紀錄
			crawldataId=mysql.insertCrawdata(json_data)
			print('Insert crawldataId: ',crawldataId)
			mysql.insertTabCrawldata(crawldataId, self.tabsId, self.name)
			mysql.PTTinsertCrawldataExtendinfo(crawldataId, json_data)
		else:
			cextendinfoId=mysql.PTTupdateCrawldataExtendinfo(crawldataId, json_data)
			print('Update extendinfoId: ', cextendinfoId)
		return 1

	def parseGos(self, link , g_id):
		# print(link)
		# self._log(link)
		resp = requests.get(url=str(link),cookies={"over18":"1"})
		soup = BeautifulSoup(resp.text,"html.parser")
		# self._log(soup.string)
		# print(resp)
		# author
		author  = soup.find(id="main-container").contents[1].contents[0].contents[1].string.replace(' ', '')
		# title
		title = soup.find(id="main-container").contents[1].contents[2].contents[1].string.replace(' ', '')
		# date
		date = soup.find(id="main-container").contents[1].contents[3].contents[1].string
		# parser_tz轉成時間物件之後在轉換timetamp
		date_timestamp = mktime_tz(parsedate_tz(date))
		# ip
		try:
			ip = soup.find(text=re.compile("※ 發信站:"))
			ip = re.search("[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*",str(ip)).group()
		except:
			ip = "ip is not find"
		# content
		a = str(soup.find(id="main-container").contents[1])
		a = a.split("</div>")
		a = a[4].split("<span class=\"f2\">※ 發信站: 批踢踢實業坊(ptt.cc),")
		content = a[0].replace(' ', '').replace('\n', '').replace('\t', '')
		# message
		num , all , g , b , n ,message = 0,0,0,0,0,{}
		for tag in soup.find_all("div","push"):
			num += 1
			push_tag = tag.find("span","push-tag").string.replace(' ', '')
			push_userid = tag.find("span","push-userid").string.replace(' ', '')
			push_content = tag.find("span","push-content").string.replace(' ', '').replace('\n', '').replace('\t', '')
			push_ipdatetime = tag.find("span","push-ipdatetime").string.replace('\n', '')

			message[num]={"狀態":push_tag,"留言者":push_userid,"留言內容":push_content,"留言時間":push_ipdatetime}
			if push_tag == '推':
				g += 1
			elif push_tag == '噓':
				b += 1
			else:
				n += 1
		messageNum = {"g":g,"b":b,"n":n,"all":num}
		# json-data
		d={ "a_ID":g_id , "b_作者":author , "c_標題":title , "d_日期":date , "d_時間戳記":date_timestamp , "e_ip":ip , "f_內文":content , "g_推文":message, "h_推文總數":messageNum, "i_連結":link }
		# https://docs.python.org/2/library/json.html
		return d


	def replace(self,pattern, subst):
		fh, abs_path = mkstemp()
		with open(abs_path,'w') as new_file:
			with open(self.targetfile) as old_file:
				for line in old_file:
					new_file.write(line.replace(pattern, subst))
		close(fh)
		remove(self.targetfile)
		move(abs_path, self.targetfile)

	def _log(self, msg):
		# return
		print( strftime("[%H:%M:%S]")+msg )

	# 當呼叫mysql形式時會將暫存Json檔目錄移除
	def delete(self):
		if os.path.exists(self.targetdir):
			shutil.rmtree(self.targetdir)
		return

if __name__ == "__main__":
	ptt_prepath = os.path.realpath(os.path.dirname(os.path.abspath(__file__))+'/../../public/temp/PTT')
	mysql = mydb()
	for item in mysql.getAllTabs(1):
		crawler = mycrawler(item)
		crawler.setStoreFilePath(ptt_prepath)
		crawler.parser('json',0,2)

	# store('[')
	# crawler(int(sys.argv[1]),int(sys.argv[2]))
	# store(']')
	# with open('data.json', 'r') as f:
	# 	p = f.read()
	# with open('data.json', 'w') as f:
	# 	f.write(p.replace(',]',']'))

# -*- coding: UTF-8 -*-
import re
import os
import sys
import json
import requests
import shutil
from os import remove, close
from tempfile import mkstemp
# python 處理Email格式時間轉換timestamp所需函式庫
from email.utils import parsedate_tz, mktime_tz, formatdate
from shutil import move
from time import sleep,strftime,strptime
from pprint import pprint
from bs4 import BeautifulSoup
from todb import todb


class Crawler(object):
    """docstring for Crawler"""
    def __init__(self, pttitem):
        super(Crawler, self).__init__()
        # self.Storepath = os.path.join(Dir , strftime("%m-%d-%H%M.json"))
        # 直接寫入mysql處理使用
        self.tabsId = pttitem['tabsId']
        # 作為Json儲存目錄使用
        self.name = pttitem['dirname']
        self.URL = pttitem['source']

    def setStoreFilePath(self, prepath):
        self.targetdir = prepath+'/'+self.name
        if not os.path.exists(self.targetdir):
            os.makedirs(self.targetdir)
            os.chmod(self.targetdir,0o777)
        self.targetfile = self.targetdir+'/'+strftime("%m-%d-%H%M.json")
        # # html
        # self.htmldir = prepath+'/html'
        # if not os.path.exists(self.htmldir):
        #     os.makedirs(self.htmldir)
        #     os.chmod(self.htmldir,0o777)
        # # json
        # self.jsondir = prepath+'/json'
        # if not os.path.exists(self.jsondir):
        #     os.makedirs(self.jsondir)
        #     os.chmod(self.jsondir,0o777)
        return 1

    def parser(self,output='json'):
        resp = requests.get(
                # url=self.URL,
                url="https://sale.591.com.tw",
                cookies={"house591": "1"}
               )
        soup = BeautifulSoup(resp.text,"html.parser")

        # self.sf=open(self.targetfile , "a")
        # self.sf.write('[')
        for tag in soup.find_all("ul","shInfo"):
            try:
                # main-link
                for mainlink in tag.find_all("a","imgbd"):
                    h_link = mainlink.get("href")
                    h_coverAlt = mainlink.contents[0].get('alt')
                    if h_coverAlt is None:
                        h_coverAlt = mainlink.contents[0].get('title')
                    h_coverSrc = mainlink.contents[0].get('src')
                    # pprint(h_coverAlt)
                    # pprint(h_coverSrc)

                # title
                for item in tag.find_all("div","right"):
                    # print(len(list(item.descendants)))
                    tmp=list(item.descendants)
                    h_title=tmp[4]
                    h_addressSeg=tmp[8].text.replace(u'\xa0','')
                    # 格局
                    h_patterns=tmp[len(tmp)-2]
                    # 用途/單價
                    ttmp=tmp[len(tmp)-4]
                    h_usefor=ttmp.split('，')[0]
                    h_singlePrice=ttmp.split('，')[1].replace(u'單價：','').replace(u'元','')
                    if ttmp.split('，')[2]:
                        h_floor = ttmp.split('，')[2]

                # convertJson
                d = {'h_link':h_link,'h_cover':h_coverSrc,'h_title':h_title,'h_addressSeg':h_addressSeg,'h_usefor':h_usefor,'h_singlePrice':h_singlePrice}
                # d=self.parseGos(link,g_id)
                # json_data=json.dumps(d,ensure_ascii=False,indent=4,sort_keys=True)+','
                # pprint(d)
                # self.sf.write(json_data)

                if( output == 'html' ):
                    formatdata=fbcon.prettify()
                    self.writeToHtml(formatdata, str(stamp))
                elif( output == 'json' ):
                    json_data.append(json_data)
                elif( output == 'mysql' ):
                    self.writeToMysql(d)

            except:
                pass
        # self.sf.write(']')
        # self.sf.close()
        # self.replace(',]',']')
        # os.chmod(self.targetfile,0o644)

        return 1


    # act1. 取得完整單筆數據內容結構 json_data
    def writeToMysql(self, json_data):
        # act2. 確認是否為重複紀錄，若是則僅更新extendinfo
        mysql = todb()
        crawldataId=mysql.existCrawlerdata(json_data, 5)
        # print("HERE: ",crawldataId)
        if not crawldataId:
            # act3. 處理寫入紀錄，包含主紀錄與延伸紀錄
            crawldataId=mysql.insertCrawdata(json_data)
            print('Insert crawldata: ',crawldataId)
            mysql.insertTabCrawldata(crawldataId, self.tabsId, self.name)
            # mysql.FBinsertCrawldataExtendinfo(crawldataId, json_data)
        # else:
            # cextendinfoId=mysql.FBupdateCrawldataExtendinfo(crawldataId, json_data)
            # print('Update: ', cextendinfoId)
        # 移除整個目標目錄
        self.delete()
        return 1

    def replace(self,pattern, subst):
        fh, abs_path = mkstemp()
        with open(abs_path,'w') as new_file:
            with open(self.targetfile) as old_file:
                for line in old_file:
                    new_file.write(line.replace(pattern, subst))
        close(fh)
        remove(self.targetfile)
        move(abs_path, self.targetfile)


    def _log(self, msg):
        # return
        print(strftime("[%H:%M:%S]")+msg)

    # 當呼叫mysql形式時會將暫存Json檔目錄移除
    def delete(self):
        if os.path.exists(self.targetdir):
            shutil.rmtree(self.targetdir)
        return

if __name__ == "__main__":
    crawler_prepath = os.path.realpath(os.path.dirname(os.path.abspath(__file__))+'/../../public/temp/591')
    mysql = todb()
    for item in mysql.getAllTabs(5):
        crawler = Crawler(item)
        crawler.setStoreFilePath(crawler_prepath)
        crawler.parser('mysql')

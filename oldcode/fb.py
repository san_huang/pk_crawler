# -*- coding: UTF-8 -*-
import os
import sys
import pprint
import time
import shutil
import random
from time import strftime
import json
import mechanicalsoup
from bs4 import BeautifulSoup
from .mydb import mydb


class Crawler:
    def __init__(self, fbitem):
        # 直接寫入mysql處理使用
        self.tabsId = fbitem['tabsId']
        # 作為Json儲存目錄使用
        self.name = fbitem['dirname']
        # URL = 'https://www.facebook.com/'+pagename
        mimes = [('User-Agent   Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4'),
                 ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36'),
                 ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:43.0) Gecko/20100101 Firefox/43.0')]

        browser = mechanicalsoup.Browser()
        # use random header try to confuse facebook defence
        browser.addheaders = [('User-agent', random.choice(mimes) )]
        # 從指定粉絲團存取目前最新的檔案！
        self.fbSource = browser.get(fbitem['source'])


    def setStoreFilePath(self, prepath):
        self.targetdir = prepath+'/'+self.name
        if not os.path.exists(self.targetdir):
            os.makedirs(self.targetdir)
            os.chmod(self.targetdir,0o777)
        # # html
        # self.htmldir = prepath+'/html'
        # if not os.path.exists(self.htmldir):
        #     os.makedirs(self.htmldir)
        #     os.chmod(self.htmldir,0o777)
        # # json
        # self.jsondir = prepath+'/json'
        # if not os.path.exists(self.jsondir):
        #     os.makedirs(self.jsondir)
        #     os.chmod(self.jsondir,0o777)
        return 1

    # 解析取回的頁面結構
    def parser(self, output):
        json_data=[]
        # 先從code class="hidden_elem"內取出註解字串！
        for comment in self.fbSource.soup.find_all('code'):
            soup = BeautifulSoup(comment.string)
            # 字串在透過BeautifulSoup過濾取出真正區塊
            contentWarp = soup.findAll('div',class_="userContentWrapper")
            if( contentWarp is not None):
                # timestamp without microtime
                stamp=int(time.time())
                for fbcon in contentWarp:
                    stamp+=1
                    if( output == 'html' ):
                        formatdata=fbcon.prettify()
                        self.writeToHtml(formatdata, str(stamp))
                    elif( output == 'json' ):
                        json_data.append(self.convertJson(fbcon))
                    elif( output == 'mysql' ):
                        self.writeToMysql(self.convertJson(fbcon))
                if(len(json_data) > 0 and output == 'json'):
                    self.writeToJson(json_data, strftime("%m-%d-%H%M.json"))
        return

    # 將解析內容依照順序寫入html
    def writeToHtml(self, data, fliename):
        f = open(self.targetdir+'/'+fliename+'.html','w')
        f.write(data)
        f.close()
        return

    # 將解析內容拆解儲存JSON格式
    def convertJson(self, blockSoap):
        # 發佈者評論
        userContent=blockSoap.find('div',class_='userContent')
        if( userContent.p is not None):
            publishContent = userContent.p.text
        else:
            publishContent = None

        shareinfo=blockSoap.find('div',class_='mbs')
        if( shareinfo is not None):
            # 原文標題
            title = shareinfo.a.text
            # 原文連結
            link = shareinfo.a['href']
        else:
            title = '無原始標題資訊'
            link = None
        # 發佈時間資訊
        abbr=blockSoap.find('span',class_='fsm fwn fcg').a.abbr
        timestamp=None
        timeformat=None
        if( abbr is not None):
            timeformat=abbr['title']
            timestamp=abbr['data-utime']
        # 分享圖資訊 shareimage['src'] / shareimage['width'] / shareimage['height']
        shareimage=blockSoap.find('img',class_='scaledImageFitWidth')
        if( shareimage is None ):
            shareimage=blockSoap.find('img',class_='scaledImageFitHeight')
            if( shareimage is None ):
                i = None
            else:
                i = {'src':shareimage['src'],'width':shareimage['width'],'height':shareimage['height']}
        else:
            i = {'src':shareimage['src'],'width':shareimage['width'],'height':shareimage['height']}

        jd = {"f_內文":publishContent,"c_標題":title,"i_連結":link,"d_時間戳記":timestamp,"d_日期":timeformat,"shareimage":i}
        return jd
        # return json.dumps(jd,ensure_ascii=False,indent=4,sort_keys=True)

    def writeToJson(self, json_data, fliename):
        f = open(self.targetdir+'/'+fliename,'w')
        f.write('[')
        for d in json_data:
            # print( 'len:',len(json_data) ,' index:',json_data.index(d) )
            jdata = json.dumps(d,ensure_ascii=False,indent=4,sort_keys=True)
            if( json_data.index(d) != (len(json_data)-1) ):
                jdata +=','
            f.write(jdata)
        f.write(']')
        f.close()
        return


    # act1. 取得完整單筆數據內容結構 json_data
    def writeToMysql(self, json_data):
        # act2. 確認是否為重複紀錄，若是則僅更新extendinfo
        mysql = mydb()
        crawldataId=mysql.existCrawlerdata(json_data, 0)
        if not crawldataId:
            # act3. 處理寫入紀錄，包含主紀錄與延伸紀錄
            crawldataId=mysql.insertCrawdata(json_data)
            print('Insert crawldata: ',crawldataId)
            mysql.insertTabCrawldata(crawldataId, self.tabsId, self.name)
            mysql.FBinsertCrawldataExtendinfo(crawldataId, json_data)
        else:
            cextendinfoId=mysql.FBupdateCrawldataExtendinfo(crawldataId, json_data)
            print('Update: ', cextendinfoId)
        # 移除整個目標目錄
        self.delete()
        return 1

    # 當呼叫mysql形式時會將暫存Json檔目錄移除
    def delete(self):
        if os.path.exists(self.targetdir):
            shutil.rmtree(self.targetdir)
        return

if __name__ == "__main__":
    fb_prepath = os.path.realpath(os.path.dirname(os.path.abspath(__file__))+'/../../public/temp/FB')
    mysql = mydb()
    for item in mysql.getAllTabs(0):
        fbcrawler = Crawler(item)
        # 呼叫mycrawler進行parser處理
        fbcrawler.setStoreFilePath(fb_prepath)
        fbcrawler.parser('mysql')
    if( mysql.close() ):
        print('done!');
